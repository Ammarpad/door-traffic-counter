# Door-Traffic-Counter
This program attempts to detect and count the number of people passing through a door. 
We compute the absolute difference between the current frame and
previous, and if changes exist, find where.
 
  Built with Python 2.7 and OpenCV
  To run, all dependencies need to be installed. You can check that with

   $ pip install --trusted-host pypi.python.org -r requirements.txt
   
  For running the program in Terminal 

   $ python DoorTrafficCounterMain.py
   
  The clip used for demonstartion can be changed and a stream from RTSP camera can be set;
   camera = cv2.VideoCapture("doorvideoexample.mp4")
